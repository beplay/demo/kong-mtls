# Kong mTLS

## Preparing cert and keystore for curl and sample Java application

1.Generate client key and cert. Then share client.crt to target.

This example generate sample consumer key and cert.

```sh
openssl req -x509 -nodes \
  -days 365 \
  -newkey rsa:2048 \
  -subj "/CN=client.com" \
  -keyout client.key \
  -out client.crt
```

View detail of cert

```sh
openssl x509 \
  --in client.crt \
  -text \
  --noout
```

2.Combine key and cert to PKCS12 file

```sh
openssl pkcs12 -export -out client.p12 -inkey client.key -in client.crt
```

3.Import client keystore (client.p12) to Java Keystore

```sh
keytool -importkeystore \
  -deststorepass changeit \
  -destkeystore app-keystore.p12 \
  -deststoretype pkcs12 \
  -srckeystore client.p12 \
  -srcstoretype pkcs12
```

4.Testing with curl

```sh
curl \
  --key client.key \
  --cert client.crt \
  https://mnp-uat.truecorp.co.th/headers
```

5.Testing with Java

```sh
java \
  -Djavax.net.ssl.keyStore=app-keystore.p12 \
  -Djavax.net.ssl.keyStorePassword=changeit \
  Https https://mnp-uat.truecorp.co.th/headers
```

---

## Enable mTLS Kong plugin

Assum that you got `consumer.crt` from your consumer.

1. Add consumer certificate file via Kong GUI

    - Select`Certificate` (Left side menu) -> Click `New Certificate` button then select `CA Certificates`
    - Copy content in `consumer.crt` to text box `Cert`
    - Kong will generate CA Certificate `id` for config with mTLS plugin.

2. Add `Mutual TLS Authentication` plugin at `route` or `service`.
    - Put the list of CA certificate `id` (Multiple `id` separate with comma `,`) from step 1 into field `Config.Ca Certificates`.
    - **NOT RECOMMENDED** Check `Config.Skip ConsumerLookup`. Cause web can't add authorization plugins.

        At this point we can test with `cURL`.

3. **RECOMMENDED** Mapping CA Certificate `id` with consumer.

    We can also add other plugin to `consumer` level. eg. `IP Restriction` or `Rate Limit` for each consumer.

    - Edit `mtls-auth` plugin by uncheck `Config.Skip ConsumerLookup`.
    - [Create mapping between CA certificate and consumer](https://docs.konghq.com/hub/kong-inc/mtls-auth/) via Admin API only

    ```sh
    KONG_ADMIN_TOKEN=xxxxx

    curl --location --request POST 'http://pilota-uat.true.th:8001/cprm-uat/consumers/<consumer-name>/mtls-auth' \
      --header 'Kong-Admin-Token: ${KONG_ADMIN_TOKEN}' \
      --header 'Content-Type: application/json' \
      --data-raw '{"subject_name": "cat.or.th"}'
    ```

---

## References

[Kong: Mutual TLS Authentication](https://docs.konghq.com/hub/kong-inc/mtls-auth/)

[HOWTO: setup mTLS with same root CA on nginx](https://www.cloudbees.com/blog/how-to-set-up-mutual-tls-authentication/)

[Mutual TLS Authentication with same CA on  NodeJS](https://codeburst.io/mutual-tls-authentication-mtls-de-mystified-11fa2a52e9cf)

[mTLS with difference CA on nginx](https://levelup.gitconnected.com/certificate-based-mutual-tls-authentication-with-nginx-57c7e693759d)

[Import key and cert to Java Keystore](https://coderwall.com/p/3t4xka/import-private-key-and-certificate-into-java-keystore)
