import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

public class Https {

  public static void main(String[] args) throws Exception {
    // String httpsURL = "https://southeastasia.api.cognitive.microsoft.com/";
    String httpsURL=args[0];
    URL myUrl = new URL(httpsURL);
    HttpsURLConnection conn = (HttpsURLConnection)myUrl.openConnection();
    
    InputStream is = null;
    InputStreamReader isr = null;
    try {
      is = conn.getInputStream();
    } catch( IOException e) {
      is = conn.getErrorStream();
    } catch(Throwable t) {
      t.printStackTrace();
    }finally {
      
    }

    isr = new InputStreamReader(is);
    BufferedReader br = new BufferedReader(isr);

    String inputLine;

    while ((inputLine = br.readLine()) != null) {
        System.out.println(inputLine);
    }

    br.close();
  }

}
